<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
        'chants_nom' => "Chants",
        'chants_slogan' => "Répertoire de chants",
        'chants_description' => "Ce plugin permet de référencer des chants (paroles, auteurs, tempo, etc.) sur la même structure qu'<a href='http://www.opensong.org/'>OpenSong</a>.",
);
?>